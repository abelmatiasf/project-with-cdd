import React from "react";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";

// 1. Creamos los estilos con makeStyles
const useStyles = makeStyles((theme) => ({
    primary: {
        textTransform: "capitalize !important",
        color: "#FFF",
        background: "#0B2B5C",
        padding: "12px 16px", // 2. Agregamos el tamaño
        borderRadius: "4px",
        gap: "8px",
        width: "100%",
        maxHeight: "47px",
    },
    secondary: {
        textTransform: "capitalize !important",
        color: "0B2B5C",
        background: "#FFF",
        border: "2px solid #0B2B5C",
        padding: "12px", // 2. Agregamos el tamaño
        borderRadius: "4px",
        gap: "8px",
        width: "100%",
        maxHeight: "47px",
    },
}));

// 3. Creamos el componente funcional
const ButtonV2 = ({ size, variant, label, ...rest }) => {
    // 4. Llamamos a los estilos
    const classes = useStyles();

    // 5. Renderizamos el componente de Material-UI Button con los estilos de la variante
    return (
        <Button size={size} className={classes[variant]} {...rest}>
            {label}
        </Button>
    );
};

// 6. Definimos los propTypes
ButtonV2.propTypes = {
    size: PropTypes.oneOf(['small', 'medium', 'large']),
    variant: PropTypes.oneOf(["primary", "secondary"]).isRequired,
    label: PropTypes.node.isRequired,
};

ButtonV2.defaultProps = {
    size: "large",
    variant: "primary",
    label: "Button"
};

export default ButtonV2;
