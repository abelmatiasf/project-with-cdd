import React from "react";
import ButtonV2 from "./ButtonV2";


// More on how to set up stories at: https://storybook.js.org/docs/writing-stories#default-export
export default {
    title: 'Component/ButtonV2',
    component: ButtonV2,
    parameters: {
        // Optional parameter to center the component in the Canvas. More info: https://storybook.js.org/docs/configure/story-layout
        layout: 'centered',
    },
    // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
    tags: ['autodocs'],
    // More on argTypes: https://storybook.js.org/docs/api/argtypes
    argTypes: {
        variant: { control: 'select', options: ["primary", "secondary"] },
        size: { control: 'select', options: ["small", "medium", "large"] },
        label: { control: "text" }
    },
};

// More on writing stories with args: https://storybook.js.org/docs/writing-stories/args
export const Primary = {
    args: {
        variant: "primary",
        label: 'Add',
    },
};

export const Secondary = {
    args: {
        variant: "secondary",
        label: 'Cancel',
    },
};

export const PrimaryLarge = {
    args: {
        variant: "primary",
        size: 'large',
        label: 'Add',
    },
};

export const PrimarySmall = {
    args: {
        variant: "primary",
        size: 'small',
        label: 'Add',
    },
};

export const SecondaryLarge = {
    args: {
        variant: "secondary",
        size: 'large',
        label: 'Cancel',
    },
};

export const SecondarySmall = {
    args: {
        variant: "secondary",
        size: 'small',
        label: 'Cancel',
    },
};
