import { ThemeProvider } from "@material-ui/core/styles";
import logo from './logo.svg';
import { theme } from './theme';

const App = () => {
  return (
    <>
      <ThemeProvider theme={theme}>
        <div className="App">
        </div>
      </ThemeProvider>
    </>
  );
}

export default App;
